/*
 Navicat Premium Data Transfer

 Source Server         : 234
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : 192.168.0.234:3306
 Source Schema         : impcsii

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 12/04/2021 14:24:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for p_activity_scene
-- ----------------------------
DROP TABLE IF EXISTS `p_activity_scene`;
CREATE TABLE `p_activity_scene`  (
  `SceneNo` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标注活动来源场景,如:\n            01:注册\n            02:转账\n            03:购买理财\n            04:分享\n            05:缴费\n            06:支付',
  `sceneDesc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `SCENECHANNELNO` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sceneName` varchar(42) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '场景名称',
  `sceneCode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '场景码',
  `CREATETIME` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `CheckStatus` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '复核状态（0：待复核，1：已通过，2：已拒绝）',
  `USERSEQ` decimal(20, 0) NULL DEFAULT NULL,
  `DEPTSEQ` decimal(20, 0) NULL DEFAULT NULL,
  `IsNormal` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N' COMMENT '是否正常（Y：正常，N：不可用）',
  `SceneType` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '场景类型  A:活动场景   E:事件场景',
  PRIMARY KEY (`SceneNo`) USING BTREE,
  UNIQUE INDEX `SCENECODE`(`sceneCode`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
